#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/slab.h> // k[m|z]alloc, k[z]free
#include <linux/fs.h> // file operations aka fops
#include <linux/mutex.h> // multi process operations

#include <linux/uaccess.h>  // would be <asm/uaccess.h> for kernel version <= 4.11 

MODULE_AUTHOR("Sergey Ivanov");
MODULE_DESCRIPTION("Simple rot13 driver that works like an infinite file, demonstrating sysfs and seeking");
MODULE_LICENSE("Dual MIT/GPL");
MODULE_VERSION("0.1");

DEFINE_MUTEX(rot13_mtx);

// here we're using sysfs, and we want to
// expose buffer size, position, bytes received and bytes read in sysfs


// reorganized to start with our data
static struct rot13_ctx {
  struct device *dev;
  int rx, tx;
  int data_size;
#define INITIAL_DATA_SIZE 256
  char *data;
} *rot13_context;

// resize the data component of a struct rot13_ctx, allowing for uninitialized
// TODO: fix ptr stuff maybe, need to sleep
int resize_rot13_data(struct rot13_ctx *p, size_t new_data_size){
  if (p->data == NULL) {
    p->data = kzalloc(new_data_size, GFP_KERNEL);
    if (!unlikely(p->data)) return -ENOMEM;
    p->data_size = new_data_size;
  } else {
    void* newbuf = kzalloc(new_data_size, GFP_KERNEL);
    if (!unlikely(newbuf)) return -ENOMEM;
    memcpy(newbuf, p->data, (new_data_size > p->data_size ? p->data_size : new_data_size));
    kfree(p->data);
    p->data = newbuf;
    p->data_size = new_data_size;
  }
  return 0;
}

// ---- sysfs handlers ----
static ssize_t rot13_data_size_show(struct device *dev, struct device_attribute *attr, char *buf);
static ssize_t rot13_data_size_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static ssize_t rot13_rx_show(struct device *dev, struct device_attribute *attr, char *buf);
static ssize_t rot13_tx_show(struct device *dev, struct device_attribute *attr, char *buf);

// these macros instantiate a `struct device_attribute dev_attr_<param>` here
// the name of the r/w callback function are <name>_show, and <name>_store
static DEVICE_ATTR_RO(rot13_rx);
static DEVICE_ATTR_RO(rot13_tx);
static DEVICE_ATTR_RW(rot13_data_size);


// character mapping of rot13
unsigned char rot13_map(unsigned char a) {
  if (a <= 'Z' && a >= 'A') {
    a += 13;
    if (a > 'Z')
      a -= 26;
  } else if (a <= 'z' && a >= 'a') {
    a += 13;
    if (a > 'z')
      a -= 26;
  }
  return a;
}

// interface for to rot13 device
static int open_rot13(struct inode *inode, struct file *filp);
static int close_rot13(struct inode *inode, struct file *filp);
static ssize_t read_rot13(struct file *filp, char __user *ubuf, size_t count, loff_t *off);
static ssize_t write_rot13(struct file *filp, const char __user *ubuf, size_t count, loff_t *off);
static loff_t llseek_rot13(struct file *filp, loff_t, int);

static const struct file_operations rot13_fops = {
  .open = open_rot13,
  .read = read_rot13,
  .write = write_rot13,
  .release = close_rot13,
  .llseek = llseek_rot13,
};

static struct miscdevice rot13_miscdev = {
  .minor = MISC_DYNAMIC_MINOR, // kernel dyanmically assigns a free minor
  .name = "rot13", // misc_register() auto-creates /dev/rot13 and entries in sysfs
  .mode = 0666,
  .fops = &rot13_fops
};

// now define our methods
static int __init rot13_init(void) {
  int ret = 0;
  struct device *dev;

  ret = misc_register(&rot13_miscdev);
  if (ret) {
    pr_notice("rot13 misc device registration failed, aborting\n");
    goto out;
  }

  dev = rot13_miscdev.this_device;
  pr_info("rot13 registered, minor# = %d - dev node is /dev/%s\n",
      rot13_miscdev.minor, rot13_miscdev.name);

  // our static memory points to kernel managed device memory
  rot13_context = devm_kzalloc(dev, sizeof(struct rot13_ctx), GFP_KERNEL);
  rot13_context->dev = dev;

  if (!unlikely(rot13_context)) // init struct
    return -ENOMEM;
  if (unlikely(resize_rot13_data(rot13_context, INITIAL_DATA_SIZE))) // init payload
    return -ENOMEM;

  if (IS_ENABLED(CONFIG_SYSFS)) {
    #define test_failure_goto(t, label) if ((t)) \
      { pr_info("device_create_file failed (%d), aborting\n", (t)); goto label; }
    ret = device_create_file(dev, &dev_attr_rot13_rx);
    test_failure_goto(ret, o3);
    ret = device_create_file(dev, &dev_attr_rot13_tx);
    test_failure_goto(ret, o2);
    ret = device_create_file(dev, &dev_attr_rot13_data_size);
    test_failure_goto(ret, o1);
  } else {
    pr_info("no sysfs accessible to rot13");
  }
  dev_dbg(dev, "driver initialized");

o1: device_remove_file(dev, &dev_attr_rot13_data_size);
o2: device_remove_file(dev, &dev_attr_rot13_tx);
o3: misc_deregister(&rot13_miscdev);
out:
  return ret;
};

static void __exit rot13_exit(void) {
  kfree(rot13_context->data);
  // clean up sysfs nodes
  device_remove_file(rot13_context->dev, &dev_attr_rot13_data_size);
  device_remove_file(rot13_context->dev, &dev_attr_rot13_tx);
  device_remove_file(rot13_context->dev, &dev_attr_rot13_rx);
  misc_deregister(&rot13_miscdev);
  pr_info("rot13 driver deregistered\n");
}

static int open_rot13(struct inode *inode, struct file *filp) {
  // if we don't want to implement seeking, we'd return nonseekable_open(inode, filp) here
  return 0;
}

static int close_rot13(struct inode *inode, struct file *filp) {
  return 0;
}

static ssize_t read_rot13(struct file *filp, char __user *ubuf, size_t count, loff_t *off) {
  int ret = count;

  if (mutex_lock_interruptible(&rot13_mtx))
    return -ERESTARTSYS;

  if (count == 0 || *off >= rot13_context->data_size) {
    dev_warn(rot13_context->dev, "nothing to read\n");
    ret = -EINVAL;
    goto out;
  }
  if (count + *off > rot13_context->data_size) {
    count = rot13_context->data_size - *off;
  }

  if (copy_to_user(ubuf, rot13_context->data + *off, count)) {
    ret = -EFAULT;
    goto out;
  }

  rot13_context->rx -= count;

out:
  mutex_unlock(&rot13_mtx);
  return ret;
}

static ssize_t write_rot13(struct file *filp, const char __user *ubuf, size_t count, loff_t *off) {
  int ret = count;
  char *start, *end; // for transforming

  if (mutex_lock_interruptible(&rot13_mtx))
    return -ERESTARTSYS;

  if (count == 0 || *off >= rot13_context->data_size) {
    dev_warn(rot13_context->dev, "nothing to write\n");
    ret = -EINVAL;
    goto out;
  }

  // is our buffer big enough?
  if (*off + count > rot13_context->data_size) {
    count = rot13_context->data_size - *off;
  }

  // copy from userspace memory into kernel memory
  if (copy_from_user(rot13_context->data + *off, ubuf, count)) {
    ret = -EFAULT;
    goto out;
  }

  // if we were doing real encryption, we'd do it on input so memory would be obfuscated
  start = rot13_context->data + *off;
  end = rot13_context->data + *off + count;
  while (start < end) {
    *start = rot13_map(*start);
    ++start;
  }

out:
  mutex_unlock(&rot13_mtx);
  return ret;
}

static loff_t llseek_rot13(struct file *filp, loff_t offset, int whence) {
  loff_t newpos;
  // todo: mention how we'd actually use *filp to track this and store data

  switch(whence) {
   case 0: /* SEEK_SET */
    newpos = offset;
    break;

   case 1: /* SEEK_CUR */
    newpos = filp->f_pos + offset;
    break;

   case 2: /* SEEK_END */
    newpos = filp->f_pos + offset;
    break;

   default: /* can't happen */
    return -EINVAL;
  }
  if (newpos<0) return -EINVAL;
  filp->f_pos = newpos;
  // TODO: resize if f_pos is now bigger
  return newpos;
}

// ---- definition of sysfs show/set methods ----
static ssize_t rot13_data_size_show(struct device *dev, struct device_attribute *attr, char *buf) {
  int n;
  if (mutex_lock_interruptible(&rot13_mtx)) return -ERESTARTSYS;
  n = rot13_context->data_size;
  mutex_unlock(&rot13_mtx);
  return n;
}

static ssize_t rot13_data_size_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count) {
  int ret = count, new_size;
  if (mutex_lock_interruptible(&rot13_mtx)) return -ERESTARTSYS;
  if (count == 0 || count > 12) return -EINVAL;
  ret = kstrtoint(buf, 0, &new_size);
  if (ret) goto out;
  if (resize_rot13_data(rot13_context, new_size)) {
    pr_info("error resizing rot13 buffer to %d\n", new_size);
    ret = -ENOMEM;
    goto out;
  }
out:
  mutex_unlock(&rot13_mtx);
  return ret;
}

static ssize_t rot13_rx_show(struct device *dev, struct device_attribute *attr, char *buf) {
  int n;
  if (mutex_lock_interruptible(&rot13_mtx)) return -ERESTARTSYS;
  n = rot13_context->rx;
  mutex_unlock(&rot13_mtx);
  return n;
}

static ssize_t rot13_tx_show(struct device *dev, struct device_attribute *attr, char *buf) {
  int n;
  if (mutex_lock_interruptible(&rot13_mtx)) return -ERESTARTSYS;
  n = rot13_context->tx;
  mutex_unlock(&rot13_mtx);
  return n;
}

module_init(rot13_init);
module_exit(rot13_exit);
