#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/slab.h> // k[m|z]alloc, k[z]free
#include <linux/fs.h> // file operations aka fops
#include <linux/mutex.h> // multi process operations

#include <linux/uaccess.h>  // would be <asm/uaccess.h> for kernel version <= 4.11


MODULE_AUTHOR("Sergey Ivanov");
MODULE_DESCRIPTION("Simple rot13 driver that works like a pipe, each character written will be encrypted and available to re-read, lack of available characters results in EOF");
MODULE_LICENSE("Dual MIT/GPL");
MODULE_VERSION("0.1");

// character mapping of rot13
unsigned char rot13_map(unsigned char a) {
  if (a <= 'Z' && a >= 'A') {
    a += 13;
    if (a > 'Z')
      a -= 26;
  } else if (a <= 'z' && a >= 'a') {
    a += 13;
    if (a > 'z')
      a -= 26;
  }
  return a;
}

// interface for to rot13 device
static int open_rot13(struct inode *inode, struct file *filp);
static int close_rot13(struct inode *inode, struct file *filp);
static ssize_t read_rot13(struct file *filp, char __user *ubuf, size_t count, loff_t *off);
static ssize_t write_rot13(struct file *filp, const char __user *ubuf, size_t count, loff_t *off);

// define our data
static struct rot13_ctx {
  struct device *dev;
  int written;
  int data_size;
  char data[];
} *rot13_context;
#define INITIAL_DATA_SIZE 256

static const struct file_operations rot13_fops = {
  .open = open_rot13,
  .read = read_rot13,
  .write = write_rot13,
  .release = close_rot13,
  .llseek = no_llseek,
};

static struct miscdevice rot13_miscdev  = {
  .minor = MISC_DYNAMIC_MINOR, // kernel dyanmically assigns a free minor
  .name = "rot13", // misc_register() auto-creates /dev/rot13 and entries in sysfs
  .mode = 0666,
  .fops = &rot13_fops
};

// now define our methods
static int __init rot13_init(void) {
  int ret = 0;
  struct device *dev;
  ret = misc_register(&rot13_miscdev);
  if (ret) {
    pr_notice("rot13 misc device registration failed, aborting\n");
    return ret;
  }

  dev = rot13_miscdev.this_device;

  pr_info("rot13 registered, minor# = %d - dev node is /dev/%s\n",
      rot13_miscdev.minor, rot13_miscdev.name);

  // our static memory points to kernel managed device memory
  rot13_context = devm_kzalloc(dev, sizeof(struct rot13_ctx) + INITIAL_DATA_SIZE, GFP_KERNEL);
  if (unlikely(!rot13_context)) return -ENOMEM;
  rot13_context->data_size = INITIAL_DATA_SIZE;
  rot13_context->dev = dev;
  dev_dbg(rot13_context->dev, "driver initialized");
  return 0;
};

static void __exit rot13_exit(void) {
  misc_deregister(&rot13_miscdev);
  pr_info("rot13 driver deregistered\n");
}

static int open_rot13(struct inode *inode, struct file *filp) {
  // if we allow seeking, use regular open
  return nonseekable_open(inode, filp);
}

static int close_rot13(struct inode *inode, struct file *filp) {
  return 0;
}

static ssize_t read_rot13(struct file *filp, char __user *ubuf, size_t count, loff_t *off) {
  int ret = count;

  if (count == 0 || rot13_context->written == 0) {
    dev_warn(rot13_context->dev, "nothing to read\n");
    ret = -EINVAL;
    goto out;
  }

  if (count > rot13_context->written)
    count = rot13_context->written;

  if (copy_to_user(ubuf, rot13_context->data, count)) {
    ret = -EFAULT;
    goto out;
  }

  rot13_context->written -= count;
  memmove(rot13_context->data, rot13_context->data + count, rot13_context->written);

out:
  return ret;
}

static ssize_t write_rot13(struct file *filp, const char __user *ubuf, size_t count, loff_t *off) {
  int ret = count;
  char *start, *end; // for transforming
  struct rot13_ctx *new_ctx; // for if we need to reallocate memory


  // is our buffer big enough?
  if (rot13_context->written + count > rot13_context->data_size) {
    rot13_context->data_size *= 2;
    pr_info("doubling rot13 buffer size to %d", rot13_context->data_size);
    new_ctx = devm_kmalloc(rot13_context->dev, sizeof(struct rot13_ctx) + rot13_context->data_size, GFP_KERNEL);
    if (unlikely(!new_ctx)) {
      ret = -ENOMEM;
      goto out;
    }

    memcpy(new_ctx, rot13_context, sizeof(struct rot13_ctx) + rot13_context->written);
    devm_kfree(rot13_context->dev, rot13_context);
    rot13_context = new_ctx;
  }

  // copy from userspace memory into kernel memory
  if (copy_from_user(rot13_context->data + rot13_context->written, ubuf, count)) {
    ret = -EFAULT;
    goto out;
  }

  // if we were doing real encryption, we'd do it on input so memory would be obfuscated
  start = rot13_context->data + rot13_context->written;
  rot13_context->written += count;
  end = rot13_context->data + rot13_context->written;
  while (start < end) {
    *start = rot13_map(*start);
    ++start;
  }

out:
  return ret;
}

module_init(rot13_init);
module_exit(rot13_exit);
